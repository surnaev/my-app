import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

const e = React.createElement;

class Timer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {seconds: 0};
    }

    tick() {
        this.setState((state, props) => ({
            seconds: state.seconds + props.step 
        }));
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render () {
        return e('div', null, 
            'Seconds: ',
            this.state.seconds
        );
    }
}



class Square extends React.Component {
  render() {
    return (
        e('button',
          {'className':'square',
           'onClick':function() { console.log('click'); }, },
         this.props.value)
    );
  }
}

class Board extends React.Component {
  renderSquare(i) {
    return e(Square, {value :i});
  }

  render() {
    const status = 'Next player: X';
    return (
      e('div',{},
          e('div', {'className':"status"}, status),
          e('div', {'className':"board-row"},
            this.renderSquare(0),
            this.renderSquare(1),
            this.renderSquare(2)
          ),
          e('div', {'className':"board-row"},
            this.renderSquare(3),
            this.renderSquare(4),
            this.renderSquare(5)
          ),
          e('div', {'className':"board-row"},
            this.renderSquare(6),
            this.renderSquare(7),
            this.renderSquare(8)
          ),
      )
    )
  }
}

class Game extends React.Component {
  render() {
    return (
      e('div', {'className':"game"},
        e('div', {'className': "game-board"},
          e(Board)
        ),
        e('div', {'className':"game-info"},
          e('div', {}),
          e('ol', {})
        ),
        e('div', {'className':"game-timer"},
            e(Timer, {'step':2})
        )
      )
    );
  }
}



// ========================================

ReactDOM.render(
  e(Game),
  document.getElementById('root')
);

